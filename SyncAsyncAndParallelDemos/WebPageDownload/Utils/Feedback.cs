﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncAsyncAndParallelDemos.WebPageDownload.Utils
{
    public static class Feedback
    {
        public static void DisplayUrlResult(string url, byte[] content)
        {
            // Display the length of each website. The string format  
            // is designed to be used with a monospaced font, such as 
            // Lucida Console or Global Monospace. 
            var bytes = content.Length;
            // Strip off the "http://".
            var displayURL = url.Replace("http://", "");
            Console.WriteLine("{0,-58} {1,8}", displayURL, bytes);
        }

        public static void DisplayTotalResult(string title, int totalBytes, long totalTimeMs)
        {
            // Display the total count for all of the web addresses.
            Console.WriteLine("{2}:: Received: {0} bytes in {1} ms\r\n", totalBytes, totalTimeMs, title);
        }
    }
}
