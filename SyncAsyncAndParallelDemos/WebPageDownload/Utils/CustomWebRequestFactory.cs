﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SyncAsyncAndParallelDemos.WebPageDownload.Utils
{
    public static class CustomWebRequestFactory
    {
        static CustomWebRequestFactory()
        {
            // http://haacked.com/archive/2004/05/15/http-web-request-expect-100-continue.aspx/
            ServicePointManager.Expect100Continue = false;  // http://alihamdar.com/2010/06/19/expect-100-continue/
            WebRequest.DefaultWebProxy = null;  // http://stackoverflow.com/questions/2519655/httpwebrequest-is-extremely-slow/3603413#3603413
            ServicePointManager.DefaultConnectionLimit = 200;
            ServicePointManager.ServerCertificateValidationCallback = AlwaysAcceptCertificate;  // http://stackoverflow.com/questions/1119347/how-to-perform-a-fast-web-request-in-c-sharp
        }

        public static HttpWebRequest Create(string url)
        {
            // http://en.code-bude.net/2013/01/21/3-things-you-should-know-to-speed-up-httpwebrequest/

            // Initialize an HttpWebRequest for the current URL. 
            var uri = new Uri(url);
            var webReq = (HttpWebRequest)WebRequest.Create(uri);
            webReq.Proxy = new WebProxy();  //  GlobalProxySelection.GetEmptyWebProxy(); // = null; // WebRequest.DefaultWebProxy; // GlobalProxySelection.GetEmptyWebProxy();  // null;  // speedups request http://stackoverflow.com/questions/2519655/httpwebrequest-is-extremely-slow
            webReq.MediaType = "HTTP/1.1";
            webReq.CookieContainer = new CookieContainer();
            webReq.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            webReq.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; de; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12";
            webReq.Referer = uri.GetLeftPart(UriPartial.Authority);  // http://stackoverflow.com/questions/14211973/get-host-domain-from-url
            webReq.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;  // http://stackoverflow.com/questions/2815721/net-is-it-possible-to-get-httpwebrequest-to-automatically-decompress-gzipd-re
            //webReq.Headers.Add("Accept-Encoding", "gzip,deflate"); // webReq.AutomaticDecompression already adds everything
            webReq.Headers.Add(HttpRequestHeader.AcceptLanguage, "en-us");
            webReq.KeepAlive = true;
            webReq.AllowReadStreamBuffering = false;

            return webReq;
        }

        private static bool AlwaysAcceptCertificate(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
