﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SyncAsyncAndParallelDemos.WebPageDownload.Utils;
using Utils.Async;

namespace SyncAsyncAndParallelDemos.WebPageDownload
{
    public static class AsyncDownloader
    {
        // There is no task return variable, T, this time because SumPageSizesSequeantalAsync doesn’t return a value for T. 
        // (The method has no Return or return statement.) However, the method must return a Task 
        // to be awaitable. Therefore, make one of the following changes
        public static async /* Task */ void SumPageSizesSequeantalAsync()
        {
            var sw = new Stopwatch();
            sw.Start();
            var totalBytes = 0;

            foreach (var url in UrlDatasource.Urls)
            {
                var urlContents = await DownloadAsync(url);
                // The previous assignment abbreviates the following two lines of code:
                // DownloadAsync returns a Task<T>. At completion, the task 
                // produces a byte array. 
                //Task<byte[]> getContentsTask = DownloadAsync(url); 
                //byte[] urlContents = await getContentsTask;

                Feedback.DisplayUrlResult(url, urlContents);

                // Update the total.
                totalBytes += urlContents.Length;
            }

            var totalTimeMs = sw.ElapsedMilliseconds;
            Feedback.DisplayTotalResult("SumPageSizesSequeantalAsync", totalBytes, totalTimeMs);
        }

        // There is no task return variable, T, this time because SumPageSizesSequeantalAsync doesn’t return a value for T. 
        // (The method has no Return or return statement.) However, the method must return a Task 
        // to be awaitable. Therefore, make one of the following changes
        public static async /* Task */ void SumPageSizesParallelAsync(int maxDegreeOfParallelism = 20)
        {
            var sw = new Stopwatch();
            sw.Start();
            var totalBytes = 0;

            await UrlDatasource.Urls.ForEachAsync(maxDegreeOfParallelism, async url =>
            {
                var urlContents = await DownloadAsync(url);
                // The previous assignment abbreviates the following two lines of code:
                // DownloadAsync returns a Task<T>. At completion, the task 
                // produces a byte array. 
                //Task<byte[]> getContentsTask = DownloadAsync(url); 
                //byte[] urlContents = await getContentsTask;

                Feedback.DisplayUrlResult(url, urlContents);

                totalBytes += urlContents.Length;
            });

            var totalTimeMs = sw.ElapsedMilliseconds;
            Feedback.DisplayTotalResult("SumPageSizesParallelAsync", totalBytes, totalTimeMs);
        }

        private static async Task<byte[]> DownloadAsync(string url)
        {
            // The downloaded resource ends up in the variable named content. 
            var content = new MemoryStream();

            var webReq = CustomWebRequestFactory.Create(url);

            // Send the request to the Internet resource and wait for 
            // the response. 
            using (var response = await webReq.GetResponseAsync())
            // The previous statement abbreviates the following two lines of code:
            //Task<WebResponse> responseTask = webReq.GetResponseAsync(); 
            //using (WebResponse response = await responseTask) 
            {
                // Get the data stream that is associated with the specified URL. 
                using (var responseStream = response.GetResponseStream())
                {
                    // Read the bytes in responseStream and copy them to content.  
                    await responseStream.CopyToAsync(content);
                    // The previous statement abbreviates the following two lines of code:
                    // CopyToAsync returns a Task, not a Task<T>. 
                    //Task copyTask = responseStream.CopyToAsync(content); 
                    // When copyTask is completed, content contains a copy of responseStream. 
                    //await copyTask;
                }
            }

            // Return the result as a byte array. 
            return content.ToArray();
        }

    }
}
