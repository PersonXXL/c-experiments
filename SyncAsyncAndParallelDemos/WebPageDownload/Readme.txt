﻿Experiment is based on MSDN articles:
http://msdn.microsoft.com/en-us/library/hh300224.aspx
http://msdn.microsoft.com/en-us/library/hh696703.aspx
http://msdn.microsoft.com/en-us/library/hh556530.aspx

Make sure to have this in your app.config to allow more than 2 connections to the same website
<system.net>
    <connectionManagement>
        <add address="*" maxconnection="200" />
    </connectionManagement>
</system.net>

You can't mark program entry point as async because of this some download methods are marked as void and don't have Task return type.

Some stats:
Total bytes returned:  2557033 bytes in 38214 ms - sync sequence maxconnection 200
Total bytes returned:  2557035 bytes in 36212 ms - sync sequence no maxconnection
Total bytes returned:  2557033 bytes in 9294 ms - sync parallel maxconnection 200 MaxDegreeOfParallelism = 20
Total bytes returned:  2557046 bytes in 33324 ms - sync parallel maxconnection 200 MaxDegreeOfParallelism = 1
Total bytes returned:  2557027 bytes in 32151 ms - sync parallel maxconnection 1 MaxDegreeOfParallelism = 20
Total bytes returned:  2557023 bytes in 19087 ms - sync parallel maxconnection 2 MaxDegreeOfParallelism = 20
Total bytes returned:  2557023 bytes in 7858 ms - sync parallel maxconnection 200 MaxDegreeOfParallelism = 20
Total bytes returned:  2557029 bytes in 33410 ms - async sequence maxconnection 200 MaxDegreeOfParallelism = 20
Total bytes returned:  2557023 bytes in 10133 ms - async parallel maxconnection 200 ForEachAsync = 20
Total bytes returned:  2557029 bytes in 8143 ms - async parallel maxconnection 200 ForEachAsync = 20
Total bytes returned:  2557029 bytes in 7803 ms - async parallel maxconnection 200 ForEachAsync = 20