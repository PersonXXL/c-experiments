﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SyncAsyncAndParallelDemos.WebPageDownload.Utils;

namespace SyncAsyncAndParallelDemos.WebPageDownload
{
    public static class SyncDownloader
    {
        private readonly static Object Locker = new Object();

        public static void SumPageSizesSequental()
        {
            var sw = new Stopwatch();
            sw.Start();
            var totalBytes = 0;

            foreach (var url in UrlDatasource.Urls)
            {
                // GetURLContents returns the contents of url as a byte array. 
                var urlContents = Download(url);

                Feedback.DisplayUrlResult(url, urlContents);

                // Update the total.
                totalBytes += urlContents.Length;
            }

            var totalTimeMs = sw.ElapsedMilliseconds;
            Feedback.DisplayTotalResult("SumPageSizesSequental", totalBytes, totalTimeMs);
        }

        /// <summary>
        /// Make sure to have:
        /* <system.net>
            <connectionManagement>
                <add address="*" maxconnection="200" />
            </connectionManagement>
        </system.net> */
        /// in App.config
        /// </summary>
        /// <param name="maxDegreeOfParallelism"></param>
        public static void SumPageSizesParallel(int maxDegreeOfParallelism = 20)
        {
            var sw = new Stopwatch();
            sw.Start();
            var totalBytes = 0;

            Parallel.ForEach(UrlDatasource.Urls, new ParallelOptions { MaxDegreeOfParallelism = maxDegreeOfParallelism }, url =>
            {
                // GetURLContents returns the contents of url as a byte array.
                var urlContents = Download(url);

                Feedback.DisplayUrlResult(url, urlContents);

                lock (Locker)
                    // Update the total.
                    totalBytes += urlContents.Length;
            });

            var totalTimeMs = sw.ElapsedMilliseconds;
            Feedback.DisplayTotalResult("SumPageSizesParallel", totalBytes, totalTimeMs);
        }

        private static byte[] Download(string url)
        {
            // The downloaded resource ends up in the variable named content. 
            var content = new MemoryStream();

            var webReq = CustomWebRequestFactory.Create(url);

            // Send the request to the Internet resource and wait for 
            // the response. 
            // Note: you can't use HttpWebRequest.GetResponse in a Windows Store app. 
            using (var response = webReq.GetResponse())
            {
                // Get the data stream that is associated with the specified URL. 
                using (var responseStream = response.GetResponseStream())
                {
                    // Read the bytes in responseStream and copy them to content.  
                    responseStream.CopyTo(content);
                }
            }

            // Return the result as a byte array. 
            return content.ToArray();
        }
    }
}
