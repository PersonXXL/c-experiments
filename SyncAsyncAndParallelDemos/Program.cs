﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SyncAsyncAndParallelDemos.WebPageDownload;

namespace SyncAsyncAndParallelDemos
{
    class Program
    {
        private static void Main(string[] args)
        {
            //SyncDownloader.SumPageSizesSequental();
            //SyncDownloader.SumPageSizesParallel();
            //AsyncDownloader.SumPageSizesSequeantalAsync();
            AsyncDownloader.SumPageSizesParallelAsync();
            
            Console.ReadLine();
        }
    }
}
