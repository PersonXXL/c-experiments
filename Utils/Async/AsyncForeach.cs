﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils.Async
{
    public static class AsyncForeach
    {
        // !!! http://stackoverflow.com/questions/14673728/run-async-method-8-times-in-parallel
        // http://stackoverflow.com/questions/19189275/asynchronously-and-parallelly-downloading-files
        // https://github.com/Q42/Q42.HueApi/blob/master/src/Q42.HueApi/Extensions/IEnumerableExtensions.cs
        // https://github.com/Q42/Q42.WinRT/blob/master/Q42.WinRT/Extensions.cs

        public static Task ForEachAsync<T>(this IEnumerable<T> source, int dop, Func<T, Task> body)
        {
            return Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext())
                            await body(partition.Current);
                }));
        }

        public static Task ForEachAsync<T, TResult>(this IEnumerable<T> source, int dop, Func<T, Task<TResult>> body)
        {
            return Task.WhenAll(
                from partition in Partitioner.Create(source).GetPartitions(dop)
                select Task.Run(async delegate
                {
                    using (partition)
                        while (partition.MoveNext())
                            await body(partition.Current);
                }));
        }
    }
}
